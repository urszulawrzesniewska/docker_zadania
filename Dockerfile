FROM ubuntu:17.10

MAINTAINER Urszula

ENV STUDENT_NAME Urszula

ENTRYPOINT ["/bin/bash", "-c", "echo Hello $STUDENT_NAME"]
